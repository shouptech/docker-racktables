# racktables-docker

This repository is used for building a docker image for Racktables.

It is based on the apache variant of the [Docker Official PHP Image](https://hub.docker.com/_/php).

To use, create a `secret.php` file, and mount it in your container at `/var/www/html/inc/secret.php`.

Example `secret.php`:

```php
<?php
$pdo_dsn = 'mysql:host=localhost;dbname=racktables';
$db_username = 'racktables';
$db_password = 'password';
$user_auth_src = 'database';
$require_local_account = TRUE;
# See https://wiki.racktables.org/index.php/RackTablesAdminGuide
?>
```

Example run:

```shell
docker run -p 8000:80 -it -v $PWD/secret.php:/var/www/html/inc/secret.php -d <image>
```

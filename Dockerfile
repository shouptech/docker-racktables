FROM php:7.4.10-apache
ADD https://github.com/RackTables/racktables/archive/RackTables-0.21.4.tar.gz /

RUN mkdir /tmp/rt \
    && cd /tmp/rt \
    && tar -zxf /RackTables-0.21.4.tar.gz --strip-components=1 \
    && mv /tmp/rt/wwwroot/* /var/www/html \
    && cd / && rm -rf /tmp/rt \
    && apt-get update && apt-get install -y \
        libfreetype6 \
        libjpeg62-turbo \
        libsnmp30 \
        libmariadb3 \
        libpci3 \
        libpng-tools \
        libpng16-16 \
        libsensors5 \
        libsnmp-base \
        libwrap0 \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libsnmp-dev \
        libldap2-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd pdo_mysql bcmath snmp ldap pcntl \
    && apt-get remove -y \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libpng-dev \
      libsnmp-dev \
      libldap2-dev \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 80
